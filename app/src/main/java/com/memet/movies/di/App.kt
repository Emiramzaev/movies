package com.memet.movies.di

import android.app.Application

class App : Application() {
    companion object {
        lateinit var appComponent: AppComponent
    }

    override fun onCreate() {
        super.onCreate()
        initializeDagger()
    }

    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
            .mvpModule(MvpModule())
            .restModule(RestModule())
            .build()
    }
}