package com.memet.movies.di

import com.memet.movies.mvp.presenter.MoviesPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MvpModule {
    @Provides
    @Singleton
    fun provideCurrenciesPresenter(): MoviesPresenter = MoviesPresenter()
}