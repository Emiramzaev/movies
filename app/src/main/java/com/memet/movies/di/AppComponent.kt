package com.memet.movies.di

import com.memet.movies.activities.MainActivity
import com.memet.movies.fragment.MoviesListFragment
import com.memet.movies.mvp.presenter.MoviesPresenter
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class))
@Singleton
interface AppComponent {

    fun inject(mainActivity: MainActivity)
    fun inject(presenter: MoviesPresenter)
    fun inject(fragment: MoviesListFragment)


}