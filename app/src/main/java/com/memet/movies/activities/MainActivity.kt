package com.memet.movies.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.memet.movies.R
import com.memet.movies.fragment.MoviesListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager
                .beginTransaction()
                .add(R.id.container, MoviesListFragment(), null)
                .commit()
        }
    }
}
