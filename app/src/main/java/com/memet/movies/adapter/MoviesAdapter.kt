package com.memet.movies.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.memet.movies.R
import kotlinx.android.synthetic.main.list_item_movi.view.*

class MoviesAdapter : BaseAdapter<MoviesAdapter.MoviesViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MoviesViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.list_item_movi, parent, false)
        return MoviesViewHolder(v)

    }


    class MoviesViewHolder(view: View) : BaseAdapter.BaseViewHolder(view) {


        init {

            itemView.setOnClickListener {

            }
        }


        override fun bind(item: Any) {
            let {
                item as Movi
                Glide.with(view.context).load(item.image).into(view.movie_image)
                view.movie_title.text = item.title
                view.movie_genre.text = item.genre.toString()
                view.movie_year.text = item.year
                view.movie_director.text = item.director
                view.movie_desription.text = item.desription

            }

        }
    }

    override fun getItemCount(): Int {
        var itemssize: Int
        if (items.size != 0) {
            itemssize = Int.MAX_VALUE
        } else {
            itemssize = items.size
        }
        return itemssize
    }

    override fun getItem(position: Int): Any {
        var post: Int
        if (position != 0) {
            post = position % items.size
        } else {
            post = position
        }
        return items[post]
    }

    data class Movi(

        var id: Int,
        var title: String,
        var year: String,
        var genre: List<String>,
        var director: String,
        var desription: String,
        var image: String
    )
}