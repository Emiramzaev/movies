package com.memet.movies.mvp.presenter

import com.memet.movies.adapter.MoviesAdapter
import com.memet.movies.di.App
import com.memet.movies.mvp.contract.MoviesContract
import com.memet.movies.rest.MoviesApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MoviesPresenter : MoviesContract.Presenter() {

    @Inject
    lateinit var moviesApi: MoviesApi

    init {
        App.appComponent.inject(this)

    }

    override fun makeList() {
        view.showProgress()

        subscribe(moviesApi.getMovies()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .flatMap { Observable.fromIterable(it) }
            .doOnNext {
                view.addMovi(
                    MoviesAdapter.Movi(
                        it.id,
                        it.title,
                        it.year,
                        it.genre,
                        it.director,
                        it.desription,
                        it.image
                    )
                )
            }
            .doOnComplete {
                view.hideProgress()
            }
            .subscribe({
                view.hideProgress()
                view.notifyAdapter()
            }, {
                view.showErrorMessage(it.message)
                view.hideProgress()
                it.printStackTrace()
            })
        )
    }

    override fun refreshList() {
        view.refresh()
        makeList()
    }
}