package com.memet.movies.mvp.contract

import com.memet.movies.adapter.MoviesAdapter

class MoviesContract {
    interface View : BaseContract.View {
        fun addMovi(movi: MoviesAdapter.Movi)
        fun notifyAdapter()
        fun showProgress()
        fun hideProgress()
        fun showErrorMessage(error: String?)
        fun refresh()
    }

    abstract class Presenter: BaseContract.Presenter<View>() {
        abstract fun makeList()
        abstract fun refreshList()
    }
}