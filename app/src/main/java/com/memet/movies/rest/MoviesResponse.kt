package com.memet.movies.rest

data class Movi (

    var id: Int,
    var title: String,
    var year: String,
    var genre: List<String>,
    var director: String,
    var desription: String,
    var image: String
)

data class MoviesList (
    var moviesList: List<Movi>
)