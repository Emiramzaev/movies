package com.memet.movies.rest

import io.reactivex.Observable
import retrofit2.http.GET


interface MoviesApi {

    @GET("movies.json ")
    fun getMovies() : Observable<List<Movi>>
}