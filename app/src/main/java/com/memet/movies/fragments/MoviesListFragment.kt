package com.memet.movies.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.memet.movies.R
import com.memet.movies.adapter.BaseAdapter
import com.memet.movies.adapter.MoviesAdapter
import com.memet.movies.di.App
import com.memet.movies.fragments.BaseListFragment
import com.memet.movies.mvp.contract.MoviesContract
import com.memet.movies.mvp.presenter.MoviesPresenter
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MoviesListFragment : BaseListFragment(), MoviesContract.View {

    @Inject
    lateinit var presenter: MoviesPresenter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        App.appComponent.inject(this)
        presenter.attach(this)
        presenter.makeList()
    }

    override fun createAdapterInstance(): BaseAdapter<*> {
        return MoviesAdapter()
    }

    override fun addMovi(movi: MoviesAdapter.Movi) {
        viewAdapter.add(movi)
    }

    override fun notifyAdapter() {
        viewAdapter.notifyDataSetChanged()
    }

    override fun showProgress() {
        requireActivity().progress.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        requireActivity().progress.visibility = View.INVISIBLE
    }

    override fun showErrorMessage(error: String?) {
        Toast.makeText(context, error, Toast.LENGTH_SHORT).show()
    }

    override fun refresh() {
        viewAdapter.items.clear()
        viewAdapter.notifyDataSetChanged()
    }

    override fun onResume() {
        super.onResume()
        presenter.attach(this)
    }

    override fun onPause() {
        super.onPause()
        presenter.detach()
    }

}